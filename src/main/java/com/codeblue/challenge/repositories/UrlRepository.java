package com.codeblue.challenge.repositories;

import com.codeblue.challenge.entities.Url;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UrlRepository extends JpaRepository<Url, Integer> {

    List<Url> findTop100ByOrderByFrequencyDesc();
}
