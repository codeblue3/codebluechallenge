package com.codeblue.challenge.controllers;

import com.codeblue.challenge.dtos.UrlAddDto;
import com.codeblue.challenge.dtos.UrlGetTopDto;
import com.codeblue.challenge.dtos.UrlPostDto;
import com.codeblue.challenge.services.UrlService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/urls")
@RequiredArgsConstructor
public class UrlController {

    private final UrlService service;
    @GetMapping("/top")
    public ResponseEntity<List<UrlGetTopDto>> getTopUrls() {
        return ResponseEntity.ok(service.getTop());
    }

    @PostMapping("/add")
    public ResponseEntity<UrlPostDto> insertUrl(@RequestBody UrlAddDto url) {
        return ResponseEntity.ok(service.postDto(url));
    }
}
