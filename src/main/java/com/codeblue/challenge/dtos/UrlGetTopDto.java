package com.codeblue.challenge.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UrlGetTopDto {

    private String url;

    private String shortedUrl;

    private int frequency;
}
