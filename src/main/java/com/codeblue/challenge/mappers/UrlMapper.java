package com.codeblue.challenge.mappers;

import com.codeblue.challenge.dtos.UrlGetTopDto;
import com.codeblue.challenge.dtos.UrlPostDto;
import com.codeblue.challenge.entities.Url;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UrlMapper {

    public List<UrlGetTopDto> urlToGetTopList(List<Url> url) {
        return url.stream().map(this::urlToGetTop).toList();
    }

    public UrlGetTopDto urlToGetTop(Url url) {
        return new UrlGetTopDto(url.getUrl(), url.getShortedUrl(), url.getFrequency());
    }

    public UrlPostDto urlToPostDto(Url url) {
        return new UrlPostDto(url.getId(), url.getUrl(), url.getShortedUrl(), url.getFrequency());
    }
}
