package com.codeblue.challenge.services;

import com.codeblue.challenge.dtos.UrlAddDto;
import com.codeblue.challenge.dtos.UrlGetTopDto;
import com.codeblue.challenge.dtos.UrlPostDto;
import com.codeblue.challenge.entities.Url;
import com.codeblue.challenge.mappers.UrlMapper;
import com.codeblue.challenge.repositories.UrlRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class UrlService {

    private final UrlRepository repository;

    private final UrlMapper mapper;

    public List<UrlGetTopDto> getTop() {
        List<Url> list = repository.findAll();
        return mapper.urlToGetTopList(repository.findTop100ByOrderByFrequencyDesc());
    }

    public UrlPostDto postDto(UrlAddDto urlDto) {
        var url = new Url();
        url.setUrl(urlDto.getUrl());
        url.setShortedUrl(generatedShortedUrl());
        var newUrl = repository.save(url);
        return mapper.urlToPostDto(newUrl);
    }

    public String generatedShortedUrl() {
        List<Url> urls = repository.findAll();
        String result = randomString(20);

        while(urls.contains(result)) {
            result = randomString(50);
        }
        return result;
    }

    private String randomString(int targetLength) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }

        return buffer.toString();
    }
}
